from enum import unique
from sys import _xoptions
import numpy as np
import networkx as nx
import copy
from sklearn.mixture import BayesianGaussianMixture, GaussianMixture
from sklearn.cluster import KMeans, AgglomerativeClustering
from scipy.optimize import linear_sum_assignment
from sklearn.metrics import pairwise_distances
import scipy as sp
import itertools
import seaborn as sns
import matplotlib.pyplot as plt

import time
import os

from karateclub import Role2Vec, Node2Vec
from graphrole import RoleExtractor, RecursiveFeatureExtractor

import argparse, logging
import numpy as np

import subprocess



class HashFunction:
    def __init__(self):
        self.reset()

    def apply(self, value):
        if value not in self.hash_dict:
            self.hash_dict[value] = self.hash_counter
            self.hash_counter += 1
        return self.hash_dict[value]

    def reset(self):
        self.hash_dict = {}
        self.hash_counter = 2


wl_hash = HashFunction()

def make_symmetric(A):
    for i in range(A.shape[0]):
        for j in range(i+1,A.shape[1]):
            A[j,i] = A[i,j]
    return A

def is_equiv_subroutine(c1, c2, return_cmap=False):
    color_map = {}
    for i in range(len(c1)):
        if c1[i] not in color_map:
            color_map[c1[i]] = c2[i]
        else:
            if color_map[c1[i]] != c2[i]:
                if return_cmap:
                    return False, color_map
                else:
                    return False
    if return_cmap:
        return True, color_map 
    else:           
        return True

def is_equivalent(c1, c2):
    return is_equiv_subroutine(c1, c2) and is_equiv_subroutine(c2, c1)

def weisfeiler_lehman(graph1: nx.Graph, iterations=-1, early_stopping=True, hash=wl_hash):
    if iterations == -1:
        iterations = len(graph1)

    Gamma1 = np.ones(len(graph1), dtype=int)
    set_colors_by_iteration = []
    colors_by_iteration = []

    for t in range(iterations):
        tmp_Gamma1 = np.copy(Gamma1)
        colors_by_iteration.append(copy.deepcopy(Gamma1))
        set_colors_by_iteration.append(set(Gamma1))
        for node in range(len(graph1)):
            Gamma1[node] = hash.apply((Gamma1[node], tuple(sorted([tmp_Gamma1[n] for n in graph1[node]]))))
        if is_equivalent(Gamma1, tmp_Gamma1) and early_stopping:
            return tmp_Gamma1, t, set_colors_by_iteration, colors_by_iteration

    colors_by_iteration.append(copy.deepcopy(Gamma1))
    set_colors_by_iteration.append(set(Gamma1))
    return Gamma1, iterations, set_colors_by_iteration, colors_by_iteration

def indicator_matrix_from_colors(colors, normalized=False, padding=0):
    if not normalized:
        H = np.array([[1 if i == colors[j] else 0 for j in range(len(colors))] for i in np.unique(colors)]).transpose()
    else:
        c, counts = np.unique(colors, return_counts=True)
        H = np.array([[1/np.sqrt(counts[j]) if colors[i] == c[j] else 0 for i in range(len(colors))] for j in range(len(c))]).transpose()
        
    return H if H.shape[1] >= padding else np.concatenate([H, np.zeros((H.shape[0], padding - H.shape[1]))], axis=1)

    

def kmedians1d(x, k, verbose=0):
    sort_idx = np.argsort(x)
    reverse = np.argsort(sort_idx)
    x = x[sort_idx]
    CC = np.zeros((len(x),len(x)))
    for i in range(CC.shape[0]):
        for j in range(i,CC.shape[1]):
            mu = np.mean([x[l] for l in range(i,j+1)])
            CC[i,j] = np.sum([np.abs(x[l]-mu) for l in range(i,j+1)])
    D = np.zeros_like(CC)
    T = np.zeros_like(CC, dtype=int)
    for i in range(CC.shape[1]):
        D[0][i] = CC[0, i]
        T[i][i] = i

    for i in range(1, k):
        for m in range(i+1,CC.shape[1]):
            idx = np.argmin([D[i-1][j-1] + CC[j,m] for j in range(i,m+1)]) + i
            D[i][m] = D[i-1][idx-1] + CC[idx,m]
            T[i][m] = int(idx) 

    if verbose >= 1:
        print(D, T)
    col = []
    k = k-1
    old_idx = len(x)-1
    cuts = [len(x)]
    for i in range(k):
        idx = T[k-i][old_idx]
        cuts.append(idx)
        old_idx = idx -1
    cuts.append(0)
    cuts.reverse()
    if verbose >= 1:
        print(cuts)
    for i in range(len(cuts)-1):
        col.extend([i]*(cuts[i+1]-cuts[i]))
    return np.array(col)[reverse]

def initialize_centers(x, num_k):
    N, D = x.shape
    centers = np.zeros((num_k, D))
    used_idx = []
    for k in range(num_k):
        idx = np.random.choice(N)
        while idx in used_idx:
            idx = np.random.choice(N)
        used_idx.append(idx)
        centers[k] = x[idx]
    return centers

def update_centers(x, r, K):
    N, D = x.shape
    centers = np.zeros((K, D))
    for k in range(K):
        centers[k] = r[:, k].dot(x) / r[:, k].sum()
    return centers

def square_dist(a, b):
    return (a - b) ** 2

def cost_func(x, r, centers, K):
    
    cost = 0
    for k in range(K):
        norm = np.linalg.norm(x - centers[k], 2)
        cost += (norm * np.expand_dims(r[:, k], axis=1) ).sum()
    return cost


def cluster_responsibilities(centers, x, beta):
    N, _ = x.shape
    K, D = centers.shape
    R = np.zeros((N, K))

    for n in range(N):        
        R[n] = np.exp(-beta * np.linalg.norm(centers - x[n], 2, axis=1)) 
    R /= R.sum(axis=1, keepdims=True)

    return R

def soft_k_means(x, K, max_iters=20, beta=1.):
    centers = initialize_centers(x, K)
    prev_cost = 0
    for _ in range(max_iters):
        r = cluster_responsibilities(centers, x, beta)
        centers = update_centers(x, r, K)
        cost = cost_func(x, r, centers, K)
        if np.abs(cost - prev_cost) < 1e-5:
            break
        prev_cost = cost
        
    return r

def directedSBM(block_size, omega):
    A = nx.to_numpy_array(nx.stochastic_block_model(block_size, make_symmetric(omega.transpose().copy()), seed=np.random.seed()))
    B = nx.to_numpy_array(nx.stochastic_block_model(block_size, make_symmetric(omega.copy()), seed=np.random.seed()))
    for i in range(B.shape[0]):
        for j in range(i, B.shape[1]):
            A[i,j] = B[i,j]
    return A

def sample_planted_role_model(c,n=[40]*3, omega_1=[], return_omega=False, verbose=0, directed=True, p_out=0.05):
    if len(omega_1) < 2:
        omega_1 = np.random.rand(len(n),len(n))
    omega = p_out * np.ones((c * len(n), c *len(n)))
    for i in range(c):
        lower = i*len(n)
        upper = (i+1) * len(n)
        omega[lower:upper, lower:upper] = omega_1
    if verbose >= 1:
        print(omega)
    if not directed:
        A = nx.to_numpy_array(nx.stochastic_block_model(n*c, make_symmetric(omega), seed=np.random.seed()))
    A = directedSBM(n*c, omega)
    labels = np.array([[i%3]*40 for i in range(9)]).flatten()
    if verbose >= 2:
        plt.plot(A @ np.ones(A.shape[1]))
        plt.show()
        plt.plot(sorted(A @ np.ones(A.shape[1])))
        plt.show()
        sns.heatmap(omega)
        plt.show()
        sns.heatmap(A)
        plt.show()
        print(omega_1)
        print(omega)
    if return_omega:
        return A, omega
    return A

def frac_WL(A, k, min_iter=2, n_iter=10, verbose=0, return_hard_coloring=False, update='mean', keep_clustering=False, early_stopping=0.1, lr=0.5, fit_function='bgm', normalize=False):
    A = A/np.max(np.abs(A))
    A_sparse = sp.sparse.csr_matrix(A)
    H = 1/k * np.ones((A.shape[1], k))
    if fit_function == 'kmeans':
        kmeans = KMeans(n_clusters=k, n_init=1)
        fit = lambda x : min([(tmp_H, frac_ep_cost(A, tmp_H)) for tmp_H in [indicator_matrix_from_colors(kmeans.fit_predict(x)) for i in range(10)]], key=lambda y : y[1])[0]
    elif fit_function == 'average_linkage':
        fit = lambda x: indicator_matrix_from_colors(AgglomerativeClustering(n_clusters=k,linkage='average').fit_predict(x))
    elif fit_function == 'soft_kmeans':
        fit = lambda x : min([(tmp_H, frac_ep_cost(A, tmp_H)) for tmp_H in [soft_k_means(x, k) for i in range(10)]], key=lambda y : y[1])[0]
    elif fit_function == 'bgm':
        g = BayesianGaussianMixture(n_components=k, n_init=1)
        fit = lambda x : min([(tmp_H, frac_ep_cost(A, tmp_H)) for tmp_H in [g.fit(X).predict_proba(X) for i in range(10)]], key=lambda y : y[1])[0]
    else:
        print('default')
        g = GaussianMixture(n_components=k, n_init=1)
        fit = lambda x : min([(tmp_H, frac_ep_cost(A, tmp_H)) for tmp_H in [g.fit(X).predict_proba(X) for i in range(10)]], key=lambda y : y[1])[0]
    if update=='mean':
        X = (1-lr) * H + lr * A_sparse @ H
    else:
        X =  A_sparse @ H
    
    gmm_params = []
    hard_coloring = [0]*A.shape[0]
    for i in range(n_iter):
        if verbose >= 4:
            sns.heatmap(X)
            plt.show()
        
        if normalize:
            X = X @ np.diag(1/np.max(X, axis=0))
        tmp = fit(X)
        if tmp.shape[1] < k:
            tmp = np.concatenate(tmp, np.ones(H.shape[0], k-tmp.shape[1]))
        
        d = pairwise_distances(tmp.transpose(), H.transpose(), metric='l2')
        if verbose >= 3:
            print("tmp")
            sns.heatmap(tmp)
            plt.show()
            print("H")
            sns.heatmap(H)
            plt.show()
        row_ind, col_ind = linear_sum_assignment(d)
        P = np.zeros((k,k))
        P[row_ind, col_ind] = [1]*k

        if verbose >= 1:
            print(frac_ep_cost(A, H), frac_ep_cost(A, tmp))
        if i > min_iter and frac_ep_cost(A, H) - frac_ep_cost(A, tmp) <= early_stopping:
            break

        H = tmp @ P
        if verbose >= 3:
            print("P")
            sns.heatmap(P)
            plt.show()
            print("H")
            sns.heatmap(H)
            plt.show()
            print("X")
            sns.heatmap(X)
            plt.show()

        if update == 'append':
            X = np.concatenate([X,A @ H], axis=1)
        if update == 'mean':
            X = (1-lr) * X + (lr) * A_sparse @ H
        else:
            X = A @ H
        if verbose >= 2:
            print("H")
            sns.heatmap(H)
            plt.show()
        
    
    if return_hard_coloring:  
        return np.argmax(H, axis=1)
    return H if H.shape[1] >= k else np.concatenate([H, np.zeros((H.shape[0], k - H.shape[1]))])

def frac_kmeans_cost_function(V, H, norm="l2"):
    D = np.diag([1/x if x != 0 else 0 for x in np.ones(H.shape[0]) @ H])
    if norm == "l1":
        return np.sum(np.abs(V - H @ D @ H.transpose() @ V))
    if norm == "l2":
        return np.sqrt(np.sum((V - H @ D @ H.transpose() @ V)**2))
    return np.linalg.norm(V - H @ D @ H.transpose() @ V, ord=norm)

def frac_ep_cost(A, H, norm='l2'):
    return frac_kmeans_cost_function(A @ H, H, norm=norm)

def hard_ep_cost(A, H):
    return frac_ep_cost(A, indicator_matrix_from_colors(colors_from_indicator_matrix(H)))

def kmeans_cost_function(V, colors, norm="l2"):
    H = indicator_matrix_from_colors(colors, normalized=True)
    if norm == "l1":
        return np.sum(np.abs(V - H @ H.transpose() @ V))
    if norm == "l2":
        return np.sum(np.power(V - H @ H.transpose() @ V,2))
    return np.linalg.norm(V - H @ H.transpose() @ V, ord=norm)

def ep_cost_function(A, colors, norm='fro'):
    return kmeans_cost_function(A @ indicator_matrix_from_colors(colors), colors, norm=norm)

def deep_ep_cost_function(A, H, alpha=1.0, depth=20, norm='l1'):
    X = copy.deepcopy(A)
    sum = 0
    for it in range(1, depth+1):
        X = X / np.linalg.norm(X)
        sum += alpha**depth * frac_ep_cost(X, H, norm=norm)
        X = X @ A
    
    return sum / depth

def frac_WL_multiter(A, k, min_iter=2, n_iter=10, verbose=0, return_hard_coloring=False, update='mean', keep_clustering=False, early_stopping=0.1, lr=0.5, fit_function='bgm', eval_func=frac_ep_cost, trials=10):
    trials = [(H, eval_func(A, H)) for H in [frac_WL(A,k, min_iter=min_iter, n_iter=n_iter, verbose=verbose, update=update, keep_clustering=keep_clustering, early_stopping=early_stopping, lr=lr, fit_function=fit_function) for t in range(trials)]]
    return sorted(trials, key=lambda x: x[1])[0][0]

def kmedians1d(x, k, verbose=0):
    sort_idx = np.argsort(x)
    reverse = np.argsort(sort_idx)
    x = x[sort_idx]
    if verbose >= 1:
        print(x)
    CC = np.zeros((len(x),len(x)))
    for i in range(CC.shape[0]):
        for j in range(i,CC.shape[1]):
            mu = np.mean([x[l] for l in range(i,j+1)])
            CC[i,j] = np.sum([np.abs(x[l]-mu) for l in range(i,j+1)])
    D = np.zeros_like(CC)
    T = np.zeros_like(CC, dtype=int)
    for i in range(CC.shape[1]):
        D[0][i] = CC[0, i]
        T[i][i] = i

    for i in range(1, k):
        for m in range(i+1,CC.shape[1]):
            idx = np.argmin([D[i-1][j-1] + CC[j,m] for j in range(i,m+1)]) + i
            #print(idx, D[i-1, idx-1], CC[idx, m])
            D[i][m] = D[i-1][idx-1] + CC[idx,m]
            T[i][m] = int(idx) 

    if verbose >= 1:
        print(D, T)
    col = []
    k = k-1
    old_idx = len(x)-1
    cuts = [len(x)]
    for i in range(k):
        idx = T[k-i][old_idx]
        cuts.append(idx)
        old_idx = idx -1
    cuts.append(0)
    cuts.reverse()
    if verbose >= 1:
        print(cuts)
    for i in range(len(cuts)-1):
        col.extend([i]*(cuts[i+1]-cuts[i]))
        if verbose >= 1:
            print(col)
    if verbose >= 1:
        print(sort_idx)
        print(reverse)
        print(np.array(col)[reverse])
    return np.array(col)[reverse]


def colors_from_indicator_matrix(H):
    return np.argmax(H, axis=1)

def assignment_acc(col, target):
    num_classes = len(np.unique(target))
    cost_matrix = np.zeros((num_classes, num_classes))
    for i in range(num_classes):
        for j in range(num_classes):
            cost_matrix[i,j] = -np.sum(np.multiply(col == i, target == j))

    row_ind, col_ind = linear_sum_assignment(cost_matrix)
    return -np.sum(cost_matrix[row_ind, col_ind])/len(col)

def evaluate_algorithm(A, k, algorithm, iterations=3, eval_function=ep_cost_function, size_H=0):
    colors = np.zeros((iterations, A.shape[0], k))
    eval = np.zeros((iterations))
    times = np.zeros((iterations))


    for it in range(iterations):
        np.random.seed()
        start = time.process_time()
        try:
            colors[it] = np.array(algorithm(A, k))
        except Exception as err:
            colors[it] = np.concatenate([np.ones((A.shape[0], 1)),np.zeros((A.shape[0], k-1))], axis=1)
            print(err)
        
        times[it] = time.process_time() - start
        eval[it] = eval_function(A,colors[it])

    if iterations == 0: #Debugging
        return 0,0,np.concatenate([np.ones((A.shape[0], 1)),np.zeros((A.shape[0], size_H-1))], axis=1)
    
    min_idx = np.argmin(eval)
    # print(eval, eval[min_idx])
    
    return eval[min_idx], np.mean(times), np.concatenate([np.array(colors[min_idx]),np.zeros((A.shape[0], size_H-k))], axis=1) if size_H > k else np.array(colors[min_idx])



def role2vec(A, k, precomputed_embedding=None, return_embedding=False):
    if precomputed_embedding is not None:
        embedding = precomputed_embedding
    else:
        emb = Role2Vec()
        emb.fit(nx.from_numpy_array(A))
        embedding = emb.get_embedding()
    if return_embedding:
        return embedding
    return indicator_matrix_from_colors(KMeans(k, n_init=5).fit_predict(embedding), padding=k)

def node2vec(A, k, precomputed_embedding=None, return_embedding=False):
    if precomputed_embedding is not None:
        embedding = precomputed_embedding
    else:
        emb = Node2Vec()
        emb.fit(nx.from_numpy_array(A))
        embedding = emb.get_embedding()
    if return_embedding:
        return embedding
    return indicator_matrix_from_colors(KMeans(k, n_init=5).fit_predict(embedding), padding=k)

def rolX(A, k, precomputed_embedding=None, return_embedding=False):
    if precomputed_embedding is not None:
        feats = precomputed_embedding
    else:
        feats = RecursiveFeatureExtractor(nx.from_numpy_array(A), max_generations=20).extract_features()
    
    if feats.shape[1] > k:
        re = RoleExtractor(n_roles=k)
        re.extract_role_factors(feats)
        cols = np.unique([re.roles[i] for i in range(A.shape[0])], return_inverse=True)[1]
    else:
        cols = KMeans(k, n_init=5).fit_predict(feats)
    if return_embedding:
        return feats
    return indicator_matrix_from_colors(cols, padding=k)

def struct2vec_emb(A, k, precomputed_embedding=None, return_embedding=False, name='graph'): 
    if precomputed_embedding is None:
        g = nx.from_numpy_array(A)
        nx.write_edgelist(g, f'graph/{name}.edgelist')
        subprocess.run(['python3', 'src/main.py', '--input', f'graph/{name}.edgelist', '--output', f'emb/{name}.emb'])
        with open(f'emb/{name}.emb', 'r') as f:
            lines = f.readlines()[1:]
        lines = {int(line.strip().split(' ')[0]) : np.array([float(x) for x in line.strip().split(' ')[1:]]) for line in lines}

        feats = [lines[i] for i in range(A.shape[0])]
    else:
        feats = precomputed_embedding
    if return_embedding:
        return feats
    else:
        return indicator_matrix_from_colors(KMeans(k, n_init=5).fit_predict(feats), padding=k)

def appr_EP_by_dom_EV(A, k, its=20, return_X=False, precomputed_embedding=None):
    A_sparse = sp.sparse.csr_matrix(A)
    if precomputed_embedding is not None:
        X = precomputed_embedding
    else:
        X = np.ones((A.shape[1], 1))
        for i in range(its):
            X = A_sparse @ X
            X = X / np.linalg.norm(X)
        X = X.reshape((A.shape[1],))
    
    if return_X:
        return indicator_matrix_from_colors(kmedians1d(X, k)), X
    return indicator_matrix_from_colors(kmedians1d(X, k), padding=k)


def save_experiment(root: str, evals, times, colors, graphs=None):
    if not os.path.exists(root):
        os.makedirs(root)
    np.save(root + '/evals', evals)
    np.save(root + '/times', times)
    np.save(root + '/colors', colors)
    if graphs is not None:
        np.save(root + '/graphs', graphs)
