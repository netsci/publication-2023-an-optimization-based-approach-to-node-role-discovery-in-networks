import main
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import cross_val_score, StratifiedShuffleSplit, GridSearchCV, StratifiedKFold
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression 
from graphrole import RoleExtractor, RecursiveFeatureExtractor
from datetime import datetime
from main import struct2vec_emb, rolX, node2vec, role2vec

def get_labels(root, node_list=None):
    with open(root, 'r') as f:
        labels = f.readlines()[1:]
    labels = np.array([label.strip().split(' ') for label in labels], dtype=int)
    if node_list is None:
        return labels[:,1]
    else:
        labels_dict = {label[0]:label[1] for label in labels}
        return np.array([labels_dict[i] for i in node_list], dtype=int)

if __name__ == '__main__':
    algs = ['deg', 'our_embedding', 'struc2vec', 'rolx', 'node2vec', 'role2vec']
    str_now = datetime.now().strftime("%Y-%m-%d_%H-%M")
    
    for data_dir in ['brazil-airports', 'europe-airports', 'usa-airports']:
        graph = nx.read_edgelist('graph/' + data_dir + '.edgelist', nodetype=int)
        node_list = sorted(list(graph.nodes()))
        A = nx.to_numpy_array(graph, nodelist=node_list)
        labels = get_labels('graph/labels-' + data_dir + '.txt', node_list=node_list)
        struc2vec_emb = struc2vec_emb('emb/' + data_dir + '.emb')
        
        rolx_emb = rolX(graph, node_list)

        
        node2vec_emb = main.node2vec(A, 0, return_embedding=True)
        role2vec_emb = main.role2vec(A, 0, return_embedding=True)

        num_classes = 40 if data_dir == 'brazil-airports' else (30 if data_dir == 'europe-airports' else 30)
        H = main.frac_WL(A, num_classes, fit_function='average_linkage',normalize=True)
        emb = A @ H 
        emb_normalized = emb @ np.diag(1/np.max(emb,axis=0))
        col = main.colors_from_indicator_matrix(H)
        
        H_ev = main.appr_EP_by_dom_EV(A, num_classes)
        col_ev = main.colors_from_indicator_matrix(H)
        emb_ev = A @ H_ev
        emb_ev_normalized = emb_ev @ np.diag(1/np.max(emb_ev,axis=0))
        
        eig = nx.eigenvector_centrality_numpy(graph)
        deg = nx.degree_centrality(graph)
        eig_centr = [eig[i] for i in node_list]
        deg_cent = [deg[i] for i in node_list]
        

        
        
        
        scores = np.zeros((len(algs), 10))
        embeddings = np.zeros((len(algs), 10, len(graph), 256))
        for it in range(10):
            for alg_i, alg in enumerate(algs):
                if alg == 'struc2vec':
                    embedding = np.array([struc2vec_emb[i] for i in node_list])
                    embeddings[alg_i, it, :, :embedding.shape[1]] = embedding 
                elif alg == 'our_embedding':
                    embedding = np.concatenate([H, H_ev, emb_normalized,emb_ev_normalized,np.array(eig_centr).reshape(emb.shape[0],1)], axis=1)#np.concatenate((H, emb, emb_normalized), axis=1)
                    embeddings[alg_i, it, :, :embedding.shape[1]] = embedding
                elif alg == 'rolx':
                    embedding = rolx_emb
                    embeddings[alg_i, it, :, :embedding.shape[1]] = embedding
                elif alg == 'deg':
                    embedding = np.array(deg_cent).reshape(emb.shape[0],1)
                    embeddings[alg_i, it, :, :embedding.shape[1]] = embedding
                elif alg == 'node2vec':
                    embedding = node2vec_emb
                    embeddings[alg_i, it, :, :embedding.shape[1]] = embedding
                elif alg == 'role2vec':
                    embedding = role2vec_emb
                    embeddings[alg_i, it, :, :embedding.shape[1]] = embedding
                else:
                    raise ValueError('Invalid algorithm name')

                lr = LogisticRegression(max_iter=1000)
                cv = GridSearchCV(lr, {'C': [0.001,0.01,0.1,1,10,100,1000], 'penalty':['l2'], 'solver':['lbfgs'], 'multi_class':['ovr']}, cv=StratifiedKFold(n_splits=5, shuffle=True), scoring='accuracy')#, 'liblinear', 'newton-cg', 'newton-cholesky'
                cv.fit(embedding, labels)
                scores[alg_i, it] = cv.score(embedding, labels)
                print(scores)
        for alg_i, alg in enumerate(algs):
            print(data_dir, alg, np.mean(scores[alg_i]), np.std(scores[alg_i]), np.max(scores[alg_i]), np.min(scores[alg_i]))
        main.save_experiment('approximateEP_experiment/experiment3/' + data_dir +'_'+str_now, scores, np.zeros((1,1)), embeddings)
        
        
        
        