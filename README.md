 An Optimization-based Approach To Node Role Discovery in Networks: Approximating Equitable Partitions
====================
[![Publication](https://img.shields.io/badge/Publication-NeurIPS23-red)](https://proceedings.neurips.cc/paper_files/paper/2023/hash/e1c73e9595126794186536cfbbed012f-Abstract-Conference.html)
[![arXiv:2401.03913](https://img.shields.io/badge/arXiv-2401.03913-red?logo=arxiv&color=b31b1b)](https://arxiv.org/abs/2305.19087)
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://git.rwth-aachen.de/netsci/publication-2023-an-optimization-based-approach-to-node-role-discovery-in-networks/blob/main/LICENSE)


Similar to community detection, partitioning the nodes of a network according to their structural roles aims to identify fundamental building blocks of a network. The found partitions can be used, e.g., to simplify descriptions of the network connectivity, to derive reduced order models for dynamical processes unfolding on processes, or as ingredients for various graph mining tasks. In this work, we offer a fresh look on the problem of role extraction and its differences to community detection and present a definition of node roles related to graph-isomorphism tests, the Weisfeiler-Leman algorithm and equitable partitions. We study two associated optimization problems (cost functions) grounded in ideas from graph isomorphism testing, and present theoretical guarantees associated to the solutions of these problems. Finally, we validate our approach via a novel "role-infused partition benchmark", a network model from which we can sample networks in which nodes are endowed with different roles in a stochastic way.

##### Usage
This repository contains all code and data to replicate the experiments from the accompanying paper. You are also invited to use the provided code and packages on your own data under the MIT license. If you publish work that partially uses our code, method or ideas, please cite the above paper.

You can find the Python Scripts and Jupyter Notebooks in the `/code` folder to replicate the experiments.
To run experiments simply install the required packages and run experiment $X$:

`python experimentX.py` 

This runs the experiment as detailed in the paper. The results are saved in *approximateEP_experiment/experimentX/*. To obtain the figures as shown in the paper, use the jupyter notebook 

`approximateEP_experiment_figures.ipynb`. 


##### Installation
All necessary packages can be installed using:

`pip install -r requirements.txt`

However, this will install the cpu version of PyTorch and PyTorch Geometric needed for the GNN baseline. If you wish to use your GPU, you should follow the installation process [here](https://pytorch.org/get-started/locally/) for PyTorch first and then install PyTorch Geometric [here](https://pytorch-geometric.readthedocs.io/en/latest/install/installation.html) and then the remaining packages.



##### Funding acknowledgments 

We acknowledge partial funding from the DFG RTG 2236 “UnRAVeL” – Uncertainty and Randomness in Algorithms, Verification and Logic.” and the Ministry of Culture and Science of North Rhine-Westphalia (NRW Ruckkehrprogramm).
